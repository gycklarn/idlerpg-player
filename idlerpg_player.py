#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.etree.ElementTree as etree
import urllib.request
import datetime
import time
import argparse
import os
import configparser
    
directory = os.getenv("HOME") + "/.config/idlerpg"
if not os.path.exists(directory):
    try: 
        os.makedirs(directory)
    except:
        print("Unable to create %s! Please make sure you have write access or create it manually." % directory)
        quit(1)

def open_xml(config, directory):
    try:
        xml_file = directory + "/character_" + config['username'] + ".xml"
        urllib.request.urlretrieve(config['xml'] + config['username'], xml_file)
    except:
    # Unable to download XML file. Use existing file if available, otherwise quit.
        if os.path.isfile(xml_file):
            print("Warning! Unable to retrieve XML file! Using cached data instead!\n")
        else:
            print("Unable to retrieve XML file! Please check character spelling and internet connection.")
            quit(1)

    tree = etree.parse(xml_file)
    root = tree.getroot()

    if not root[0].text:
        print("No XML data received. This might mean the character '%s' doesn't exist or the server is having some issues." % config['username'] )

    return root

def get_args(config):
    parser = argparse.ArgumentParser(
        description="Shows IdleRPG player information",
        epilog="All arguments are optional. If run without arguments, a folder will be created in your home directory's config directory with a configuration file that you can use instead, if one doesn't already exist.")
    parser.add_argument(
            "-u",
            "--username",
            default=config['username'],
            help="Override configuration file username"
            )
    parser.add_argument(
            "-x",
            "--xml",
            default=config['baseurl'],
            help="Override configuration file base URL for the XML file"
            )
    parser.add_argument(
            "-r",
            "--repeat",
            action='store_true',
            dest="repeat",
            help="Automatically refreshes data."
            )
    parser.add_argument(
            "-d",
            "--delay",
            type=float,
            default=config['delay'],
            help="Set repeat delay timer in seconds. Default is 60"
            )
    parser.add_argument(
            "-c",
            "--clear",
            action="store_true",
            dest="clear",
            help="Automatically clears terminal window on refresh"
            )

    # Return arguments as dict instead of namespace.
    # This is necessary because the .ini parser uses dicts and I'm a lazy bastard.
    return vars(parser.parse_args())

def get_config():
    config_file = os.getenv("HOME") + "/.config/idlerpg/idlerpg.ini"
    config = configparser.ConfigParser()

    if not os.path.exists(config_file):
    # Configuration file does not exist. Create it.
        # Require username
        username = ''
        while username is '':
            username    = input("Enter your character name: ")

        # Set delay to 60 if user leaves blank
        delay       = input("Enter repeat delay in seconds [60]: ") or '60'
        
        config['idlerpg'] = {}
        config['idlerpg']['username']   = username
        config['idlerpg']['baseurl']    = 'http://xethron.lolhosting.net/xml.php?player='
        config['idlerpg']['delay']      = delay
        
        with open (config_file, 'w') as new_config:
            config.write(new_config)

    # Load data from config file.
    config.read(config_file)
    options = config.options("idlerpg")

    config_d = {}
    for option in config['idlerpg']:
        config_d[option] = config['idlerpg'][option]

    return config_d


def calc_date(ttl):
    now = int(time.time())
    future = datetime.datetime.fromtimestamp(now + int(ttl))
    return future.strftime('%Y-%m-%d %H:%M:%S')

# Player information
def get_player(root):
    player_d = {}
    for entry in root[:11]:
        player_d[entry.tag] = entry.text

    # Make time entries easier to read for the human eye
    player_d['level_date']      = calc_date(player_d['ttl'])
    player_d['level_remaining'] = str(datetime.timedelta(seconds=int(player_d['ttl'])))
    player_d['idle_time']       = str(datetime.timedelta(seconds=int(player_d['totalidled'])))

    # Store "online" in green and "offline" in red
    if player_d['online'] == '1':   player_d['status'] = "\033[92mOnline\033[0m"
    else:                           player_d['status'] = "\033[91mOffline\033[0m"

    # Make alignment readable
    if player_d['alignment']    == "g": player_d['alignment'] = "Good"
    elif player_d['alignment']  == "n": player_d['alignment'] = "Neutral"
    elif player_d['alignment']  == "e": player_d['alignment'] = "Evil"
    else:                               player_d['alignment'] = "Unknown"

    return player_d

# Penalties
def get_penalty(root):
    penalty_d   = {}
    penalties   = root[11]
    for penalty in penalties:
        penalty_d[penalty.tag] = penalty.text

    penalty_d['total']  = str(datetime.timedelta(seconds=int(penalty_d['total'])))

    return penalty_d

# Items
def get_item(root):
    items   = root[12]
    item_d  = {}
    for item in items:
        item_d[item.tag] = item.text

    return item_d

# Justify function

filler = '.' # This character is used as filler for justifying the output.
             # Example: If the filler is a dot (.), the output will look like this:
             # Character.....:
             # Replace it with a space if you think it get's too cluttery.

def lj(string):
    return (string.ljust(14, filler) + ':')

# Print character info

def present_data(player, penalty, item):
    print(lj('Character'),  '%s' % player['username'])
    print(lj('Class'),      '%s' % player['class'])
    print(lj('Level'),      '%s' % player['level'])
    print(lj('Alignment'),  '%s' % player['alignment'])
    print(lj('Status'),     '%s' % player['status'])
    print(lj('Next level'), '%s' % player['level_date'])
    print(lj('Remaining'),  '%s' % player['level_remaining'])
    print(lj('Total idled'),'%s' % player['idle_time'])
    print(lj('Penalty'),    '%s' % penalty['total'])
    #print(lj('Host'),      '%s' % player['userhost'])
    print()
    print("Items:")
    print(lj('Weapon'),     '%s' % item['weapon'])
    print(lj('Tunic'),      '%s' % item['tunic'])
    print(lj('Shield'),     '%s' % item['shield'])
    print(lj('Leggings'),   '%s' % item['leggings'])
    print(lj('Ring'),       '%s' % item['ring'])
    print(lj('Gloves'),     '%s' % item['gloves'])
    print(lj('Boots'),      '%s' % item['boots'])
    print(lj('Helm'),       '%s' % item['helm'])
    print(lj('Charm'),      '%s' % item['charm'])
    print(lj('Amulet'),     '%s' % item['amulet'])
    print()
    print(lj('Item sum'),   '%s' % item['total'])

# Main
pre_config  = get_config()          # Read .ini file.
config      = get_args(pre_config)  # Read arguments from command line.

while True:
    try:

        # Fetch and build data
        root    = open_xml(config, directory)
        player  = get_player(root)
        penalty = get_penalty(root)
        item    = get_item(root)

        # Clear screen
        if config['clear']:
            os.system('cls' if os.name == 'nt' else 'clear')

        # Show info
        present_data(player, penalty, item)

    except KeyboardInterrupt:
        quit(0)

    except:
        pass

    # Automatically refreshes on -r
    if not config['repeat']: break

    try:
        time.sleep(float(config['delay']))
        
    except KeyboardInterrupt:
        quit(0)
